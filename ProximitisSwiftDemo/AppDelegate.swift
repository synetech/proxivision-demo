//
//  AppDelegate.swift
//  ProximitisSwiftDemo
//
//  Created by Jiří Markalous on 15/09/2019.
//  Copyright © 2019 Proximitis s.r.o. All rights reserved.
//

import ProximitisFramework
import SynevisionExposition
import UIKit
import UserNotifications

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?

    private var synevisionExposition: SynevisionExpositionInit?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

        Proximitis.shared.start(with: "proximitis")
        Proximitis.setDebugging(true)

        synevisionExposition = SynevisionExpositionInit(configuration: SynevisionExpositionConfiguration.getConfiguration())

        // NOTE: Momentálně tu nechávám okamžité spuštění Synevision AR screeny po startu aplikace pro testovací účely.
        // TODO: Přesuň `.start()` tak, aby byl zavolán v reakci na event Proximitis SDK. Se SynevisionExposition není potřeba dělat nic víc než spustit `start()` a předat navigation controller ve kterém se má spustit.
        let window = UIWindow()
        let navigationController = UINavigationController()
        window.rootViewController = navigationController
        self.window = window
        synevisionExposition?.start(in: navigationController)

        UNUserNotificationCenter.current().delegate = self

        return true
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        
        Proximitis.applicationDidEnterBackground()
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        
        Proximitis.applicationDidBecomeActive()
    }
    
}


extension AppDelegate: UNUserNotificationCenterDelegate {
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
        ProximitisNotificationClient.notificationTapped(response: response)
        
        completionHandler()
    }
    
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        completionHandler([.alert, .sound])
    }
}


extension AppDelegate {
    
    func application(_ application: UIApplication, performFetchWithCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
        Proximitis.shared.performBackgroundFetch {
            completionHandler(.newData)
        }
    }
}

