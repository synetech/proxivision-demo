//
//  SynevisionExpositionConfiguration.swift
//  ProximitisSwiftDemo
//
//  Created by Lukáš Růžička on 14/02/2020.
//  Copyright © 2020 Proximitis s.r.o. All rights reserved.
//

import SynevisionExposition

class SynevisionExpositionConfiguration {

    static func getConfiguration() -> SEConfiguration {
        // NOTE: Just for case of the quick demo
        return SEConfiguration(translations: SETranslations(initialInstructionsIntroduction: "Najdi obrazy, kolem kterých jsi prošel, když ti přišla notifikace",
                                                            initialInstructionsHowToUse: "Namiř zařízením na jeden z těchto obrazů a sleduj kouzlo 🧙🏼‍♂️",
                                                            initialInstructionsPanel: "Namiř zařízením na jeden z obrazů, kolem kterých jsi prošel",
                                                            loadingReferenceImages: "Načítají se obrazy k rozpoznání..",
                                                            multipleVideosHandlingInstructions: "Pokud je ve scéně více videí, zvuk je možné přepnout kliknutím na příslušné video",
                                                            noActionForLongTimeReminder: "Rozšířená realita je náročná na výkon a proto, pokud ji nepoužíváš, raději ji zavři",
                                                            tutorialRepetitionOffer: "Nedaří se ti zprovoznit rozšířenou realitu? Zkus si znovu pustit tutoriál.",
                                                            recommendationForExcessiveMotion: "Děláš moc prudké pohyby, zkus trochu zpomalit",
                                                            recommendationForInsufficientFeatures: "Máme málo světla, zkus rozsvítit"))
    }
}
